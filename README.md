# Information systems and data base
### DB: PostgreSQL (Labs were done in russian)

## Lab1
To perform laboratory work No. 1, you must :

- Based on the proposed subject area (text), write a description of it. From the resulting description, select entities, their attributes and relationships.
- Create an infological model.
- Create a data model. When describing data types for attributes, types from the PostgreSQL DBMS must be used.
- Implement a datalogical model in PostgreSQL. When describing and implementing a datalogical model, integrity constraints that are characteristic of the resulting subject area should be taken into account.
- Fill in the created tables with test data.
Each student has their own schema for creating database objects. The schema name corresponds to the username in the studs database (sXXXXXX). Command to connect to studs database:

psql -h pg -d studs

Each student must use his own scheme when working on laboratory work No. 1 (as well as within the framework of completing stages 2, 3 and 4 of the course work).

The lab report should contain:

- Task text.
- Description of the subject area.
- List of entities and their classification (pivot, association, characteristic).
- Infological model (ER-diagram in extended form - with attributes, keys...).
- Datalogical model (should contain attribute types, auxiliary tables for displaying many-to-many relationships).
- Implementation of a datalogical model in SQL.
- Work conclusions.

Topics for preparing for the defense of laboratory work:

- ANSI-SPARC architecture
- Model "Entity-Relationship". Entity classification. Types of connections. integrity constraints.
- DDL
- DML

## Lab2

For the relations obtained during the construction of the subject area from laboratory work No. 1, do the following: 
- describe the functional dependencies for the relations of the resulting scheme (the minimum set); 
- bring relations into 3NF (at least). Build a circuit based on NF (at least). Build a diagram based on the resulting relationships; 
- Describe the changes in functional dependencies since the conversion to 3NF (as a minimum). Build a circuit based on NF; 
- convert relations to BCNF. Prove that the resulting ratios are in BCNF; 
- what denormalizations would be useful for your schema? Give a detailed description;

# Instructions for 3-4 labs:

According to the version given by the teacher, compose and execute queries to the database "Educational process".
Command to connect to ucheb database:
psql -h pg -d ucheb 


## Lab3

Topics for preparing for the defense of laboratory work:

- SQL
- Joining tables
- Subqueries

### Task:

Write queries in SQL language (points 1-7).

1. Make a query to get attributes from the specified tables, applying filters according to the specified conditions:
- Tables: N_ESTIMATES, N_SHEET.
- Output attributes: N_ESTIMATES.NOTE, N_SHEET.MEMBER_ID.
- Filters (AND):
- H_ESTIMATES.CODE = 2.
- N_WORK.ID < 1426978.
- N_WORK.ID < 1250972.
- Join type: RIGHT JOIN.
2. Make a query to get attributes from the specified tables, applying filters according to the specified conditions:
- Tables: N_PEOPLE, N_REPORT, N_SESSION.
-  Output attributes: N_PEOPLE.NAME, N_REPORT.MEMBER_ID, N_SESSION.MEMBER_ID.
- Filters (AND):
- N_PEOPLE.ID = 142095.
- N_REPORT.PERSON_ID > 153285.
- Join type: INNER JOIN.
3. Output the number of last names and patronymics, excluding repetitions.
When compiling a query, you cannot use DISTINCT.
4. Find groups in which in 2011 there were less than 10 students enrolled at FCTIU.
For implementation, use a subquery.
5. Output a table with the average age of students in all groups (Group, Average Age), where the average age is equal to the average age in group 1100.
6. Get a list of students expelled after September 1, 2012 from full-time education (specialty: 230101). Include in the result:
group number;
number, surname, name and patronymic of the student;
order item number;
For implementation, use a subquery with IN.
7. Display a list of people who are not or were not students of St. Petersburg State University ITMO (data about which are not available in the N_STUDENTS table). You cannot use DISTINCT in a query. 

## Lab4

Topics for preparing for the defense of laboratory work:

- Indices
- Query Optimization
- Choosing a Query Execution Plan

### Task: 
Write queries in SQL language (points 1-2).

For each query, suggest indexes, the addition of which will reduce the query execution time (indicate the tables/attributes for which you want to add indexes, write the index type; explain why adding an index would be useful for this query).

For requests 1-2, it is necessary to draw up possible plans for executing requests. Plans are made on the assumption that there are no indexes in the tables. From the plans drawn up, you must choose the best one and explain your choice.
Will plans change when adding an index, and how?

For queries 1-2, you need to add the output of the EXPLAIN ANALYZE [query] command to the report

Detailed answers to all of the above questions should be included in the report (query execution plans should be drawn, answers to questions should be presented in text form).

1. Make a query to get attributes from the specified tables, applying filters according to the specified conditions:
- Tables: N_PEOPLE, N_SESSION.
- Display attributes: N_PEOPLE.ID, N_SESSION.DATE.
Filters (AND):
- N_PEOPLE.ID < 100865.
- N_SESSION.ID = 32199.
- N_SESSION.ID < 14369.
Join type: INNER JOIN.
2. Make a query to get attributes from the specified tables, applying filters according to the specified conditions:
- Tables: N_PEOPLE, N_REPORT, N_SESSION.
- Display attributes: N_PEOPLE.NAME, N_REPORT.DATE, N_SESSION.DATE.
Filters (AND):
- N_PEOPLE.ID = 142095.
- N_WORK.ID > 1250972.
- Join type: INNER JOIN.

Link to labs: https://se.ifmo.ru/courses/db#labs
