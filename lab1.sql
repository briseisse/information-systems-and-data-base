CREATE TABLE Human
(
  ID			SERIAL PRIMARY KEY,
  SURNAME		VARCHAR(32) NOT NULL,
  Name		    VARCHAR(32) NOT NULL,
  COUNTRY		VARCHAR(32),
  AGE     		INTEGER NOT NULL
                CHECK ( AGE >= 16 ),
  WEIGHT_Kg  		FLOAT NOT NULL CHECK ( WEIGHT_Kg >= 50 )
);


CREATE TABLE Volcano
(
  ID         	SERIAL PRIMARY KEY,
  Name       	VARCHAR(32) NOT NULL,
  Type       	VARCHAR(32) NOT NULL
);

CREATE TABLE Objects
(
  ID		        SERIAL PRIMARY KEY,
  Name              VARCHAR(32) NOT NULL,
  Weight_g          FLOAT DEFAULT 0.0,
  Thickness_mm      FLOAT
);



CREATE TABLE Crater
(
  ID		SERIAL PRIMARY KEY,
  TEXTURE	VARCHAR(32),
  COLOR	    VARCHAR(32),
  ID_Volcan	INTEGER REFERENCES Volcano ON DELETE CASCADE
);

CREATE TABLE Expedition
(
  ID_Who			INTEGER REFERENCES Human ON DELETE CASCADE ,
  ID_Where			INTEGER REFERENCES Volcano,
  PRIMARY KEY		(ID_Who, ID_Where),
  DURATION_DAYS		INTEGER DEFAULT 3
);

CREATE TABLE Wind
(
    ID_Object		INTEGER REFERENCES Objects,
    ID_Where		INTEGER REFERENCES Crater,
    PRIMARY KEY (ID_Object, ID_Where)
);

INSERT INTO Human (SURNAME, name, AGE, COUNTRY, WEIGHT_Kg)
VALUES ('Peter', 'Jackson', 25, 'USA', 70),
       ('Eunice', 'W.', 20, 'DRC', 63),
       ('Ivan', 'Ivanov', 23, 'RUSSIA', 67),
       ('Laura', 'Akssys', 21, 'NIGERIA', 62);

INSERT INTO Objects (Name, Weight_g, Thickness_mm)
VALUES ('Soil',0 ,1),
	('Leaves', 0.5,0.11),
       ('Stone', 1,0);


INSERT INTO Volcano (NAME, type)
VALUES ('Nyirangongo', 'Active'),
       ('Cara', 'Extinct'),
       ('Etna', 'Active');

INSERT INTO Crater (TEXTURE, COLOR, ID_Volcan)
VALUES ('Steep', 'black', 1),
       ('Smooth', 'black', 3),
       ('Steep and smooth', 'black', 2);

INSERT INTO Expedition (id_Who, id_Where, duration_days)
VALUES (3, 2, 16),
       (4, 2, 5),
       (1, 2, 15);

INSERT INTO Wind (id_object, id_where)
VALUES (1, 2),
       (2, 2),
       (3, 2);
